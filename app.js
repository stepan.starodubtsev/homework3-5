// 1 Task

const quotientOperation = (arr) => arr[0] % arr[1];
const getTwoOperands = () => {
    let number;
    let numbersArr = [];
    do {
        number = parseInt(prompt("Введіть число:"));
        if (!isNaN(number)) {
            numbersArr.push(number);
            if (numbersArr.length === 2) {
                break;
            }
        }
    } while (true);
    return numbersArr;
};

console.log("Task 1: " + quotientOperation(getTwoOperands()));

// 2 Task

const getOperand = () => {
    do {
        let operator = prompt("Введіть дію(+, -, /, *):");
        if (operator === "+" || operator === "-" || operator === "*" || operator === "/") {
            return operator;
        } else {
            alert("Такої операції не існує");
        }
    } while (true)
};

const calculateTwoNumbers = (twoOperands, operation) => {
    switch (operation) {
        case "+":
            return twoOperands[0] + twoOperands[1];
        case "-":
            return twoOperands[0] - twoOperands[1];
        case "*":
            return twoOperands[0] * twoOperands[1];
        case "/":
            return twoOperands[0] / twoOperands[1];
        default:
            return null;
    }
}

console.log("Task 2: " + calculateTwoNumbers(getTwoOperands(), getOperand()));

// 3 Task

const getNumber = () => {
    do {
        let number = parseInt(prompt("Введіть додатнє число:"));
        if (!isNaN(number && number >= 0)) {
            return number;
        }
    } while (true);
};

const calculateFactorial = (number) => {
    if (number === 0 || number === 1) {
        return 1;
    }
    let factorial = 1;
    for (let i = 2; i <= number; i++) {
        factorial *= i;
    }
    return factorial;
}

console.log("Task3:", calculateFactorial(getNumber()));